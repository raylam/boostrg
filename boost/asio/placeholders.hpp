//
// placeholders.hpp
// ~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2012 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef BOOST_ASIO_PLACEHOLDERS_HPP
#define BOOST_ASIO_PLACEHOLDERS_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <boost/asio/detail/config.hpp>
#include <boost/bind/arg.hpp>
#include <boost/detail/workaround.hpp>

#include <boost/asio/detail/push_options.hpp>

namespace boostRG {
namespace asio {
namespace placeholders {

#if defined(GENERATING_DOCUMENTATION)

/// An argument placeholder, for use with boostRG::bind(), that corresponds to
/// the error argument of a handler for any of the asynchronous functions.
unspecified error;

/// An argument placeholder, for use with boostRG::bind(), that corresponds to
/// the bytes_transferred argument of a handler for asynchronous functions such
/// as boostRG::asio::basic_stream_socket::async_write_some or
/// boostRG::asio::async_write.
unspecified bytes_transferred;

/// An argument placeholder, for use with boostRG::bind(), that corresponds to
/// the iterator argument of a handler for asynchronous functions such as
/// boostRG::asio::basic_resolver::async_resolve.
unspecified iterator;

/// An argument placeholder, for use with boostRG::bind(), that corresponds to
/// the signal_number argument of a handler for asynchronous functions such as
/// boostRG::asio::signal_set::async_wait.
unspecified signal_number;

#elif defined(__BORLANDC__) || defined(__GNUC__)

inline boostRG::arg<1> error()
{
  return boostRG::arg<1>();
}

inline boostRG::arg<2> bytes_transferred()
{
  return boostRG::arg<2>();
}

inline boostRG::arg<2> iterator()
{
  return boostRG::arg<2>();
}

inline boostRG::arg<2> signal_number()
{
  return boostRG::arg<2>();
}

#else

namespace detail
{
  template <int Number>
  struct placeholder
  {
    static boostRG::arg<Number>& get()
    {
      static boostRG::arg<Number> result;
      return result;
    }
  };
}

#if BOOST_WORKAROUND(BOOST_MSVC, < 1400)

static boostRG::arg<1>& error
  = boostRG::asio::placeholders::detail::placeholder<1>::get();
static boostRG::arg<2>& bytes_transferred
  = boostRG::asio::placeholders::detail::placeholder<2>::get();
static boostRG::arg<2>& iterator
  = boostRG::asio::placeholders::detail::placeholder<2>::get();
static boostRG::arg<2>& signal_number
  = boostRG::asio::placeholders::detail::placeholder<2>::get();

#else

namespace
{
  boostRG::arg<1>& error
    = boostRG::asio::placeholders::detail::placeholder<1>::get();
  boostRG::arg<2>& bytes_transferred
    = boostRG::asio::placeholders::detail::placeholder<2>::get();
  boostRG::arg<2>& iterator
    = boostRG::asio::placeholders::detail::placeholder<2>::get();
  boostRG::arg<2>& signal_number
    = boostRG::asio::placeholders::detail::placeholder<2>::get();
} // namespace

#endif

#endif

} // namespace placeholders
} // namespace asio
} // namespace boostRG

#include <boost/asio/detail/pop_options.hpp>

#endif // BOOST_ASIO_PLACEHOLDERS_HPP
