#ifndef BOOST_BIND_PLACEHOLDERS_HPP_INCLUDED
#define BOOST_BIND_PLACEHOLDERS_HPP_INCLUDED

// MS compatible compilers support #pragma once

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

//
//  bind/placeholders.hpp - _N definitions
//
//  Copyright (c) 2002 Peter Dimov and Multi Media Ltd.
//
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//
//  See http://www.boost.org/libs/bind/bind.html for documentation.
//

#include <boost/bind/arg.hpp>
#include <boost/config.hpp>

namespace
{

#if defined(__BORLANDC__) || defined(__GNUC__) && (__GNUC__ < 4)

static inline boostRG::arg<1> _1() { return boostRG::arg<1>(); }
static inline boostRG::arg<2> _2() { return boostRG::arg<2>(); }
static inline boostRG::arg<3> _3() { return boostRG::arg<3>(); }
static inline boostRG::arg<4> _4() { return boostRG::arg<4>(); }
static inline boostRG::arg<5> _5() { return boostRG::arg<5>(); }
static inline boostRG::arg<6> _6() { return boostRG::arg<6>(); }
static inline boostRG::arg<7> _7() { return boostRG::arg<7>(); }
static inline boostRG::arg<8> _8() { return boostRG::arg<8>(); }
static inline boostRG::arg<9> _9() { return boostRG::arg<9>(); }

#elif defined(BOOST_MSVC) || (defined(__DECCXX_VER) && __DECCXX_VER <= 60590031) || defined(__MWERKS__) || \
    defined(__GNUC__) && (__GNUC__ == 4 && __GNUC_MINOR__ < 2)  

static boostRG::arg<1> _1;
static boostRG::arg<2> _2;
static boostRG::arg<3> _3;
static boostRG::arg<4> _4;
static boostRG::arg<5> _5;
static boostRG::arg<6> _6;
static boostRG::arg<7> _7;
static boostRG::arg<8> _8;
static boostRG::arg<9> _9;

#else

boostRG::arg<1> _1;
boostRG::arg<2> _2;
boostRG::arg<3> _3;
boostRG::arg<4> _4;
boostRG::arg<5> _5;
boostRG::arg<6> _6;
boostRG::arg<7> _7;
boostRG::arg<8> _8;
boostRG::arg<9> _9;

#endif

} // unnamed namespace

#endif // #ifndef BOOST_BIND_PLACEHOLDERS_HPP_INCLUDED
