//////////////////////////////////////////////////////////////////////////////
//
// (C) Copyright Ion Gaztanaga 2010-2012.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)
//
// See http://www.boost.org/libs/move for documentation.
//
//////////////////////////////////////////////////////////////////////////////

//! \file

#ifndef BOOST_INTERPROCESS_DETAIL_MOVE_HPP
#define BOOST_INTERPROCESS_DETAIL_MOVE_HPP

#include <boost/move/move.hpp>

namespace boostRG {
namespace interprocess {

using ::boostRG::move;
using ::boostRG::forward;

}  //namespace interprocess {
}  //namespace boostRG {

#endif //#ifndef BOOST_INTERPROCESS_DETAIL_MOVE_HPP
