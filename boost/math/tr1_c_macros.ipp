// Copyright John Maddock 2008-11.
// Use, modification and distribution are subject to the
// Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt
// or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_MATH_C_MACROS_IPP
#define BOOST_MATH_C_MACROS_IPP

// C99 Functions:
#ifdef acosh
#undef acosh
#endif
#define acosh boostRG_acosh
#ifdef acoshf
#undef acoshf
#endif
#define acoshf boostRG_acoshf
#ifdef acoshl
#undef acoshl
#endif
#define acoshl boostRG_acoshl

#ifdef asinh
#undef asinh
#endif
#define asinh boostRG_asinh
#ifdef asinhf
#undef asinhf
#endif
#define asinhf boostRG_asinhf
#ifdef asinhl
#undef asinhl
#endif
#define asinhl boostRG_asinhl

#ifdef atanh
#undef atanh
#endif
#define atanh boostRG_atanh
#ifdef atanhf
#undef atanhf
#endif
#define atanhf boostRG_atanhf
#ifdef atanhl
#undef atanhl
#endif
#define atanhl boostRG_atanhl

#ifdef cbrt
#undef cbrt
#endif
#define cbrt boostRG_cbrt
#ifdef cbrtf
#undef cbrtf
#endif
#define cbrtf boostRG_cbrtf
#ifdef cbrtl
#undef cbrtl
#endif
#define cbrtl boostRG_cbrtl

#ifdef copysign
#undef copysign
#endif
#define copysign boostRG_copysign
#ifdef copysignf
#undef copysignf
#endif
#define copysignf boostRG_copysignf
#ifdef copysignl
#undef copysignl
#endif
#define copysignl boostRG_copysignl

#ifdef erf
#undef erf
#endif
#define erf boostRG_erf
#ifdef erff
#undef erff
#endif
#define erff boostRG_erff
#ifdef erfl
#undef erfl
#endif
#define erfl boostRG_erfl

#ifdef erfc
#undef erfc
#endif
#define erfc boostRG_erfc
#ifdef erfcf
#undef erfcf
#endif
#define erfcf boostRG_erfcf
#ifdef erfcl
#undef erfcl
#endif
#define erfcl boostRG_erfcl

#if 0
#ifdef exp2
#undef exp2
#endif
#define exp2 boostRG_exp2
#ifdef exp2f
#undef exp2f
#endif
#define exp2f boostRG_exp2f
#ifdef exp2l
#undef exp2l
#endif
#define exp2l boostRG_exp2l
#endif

#ifdef expm1
#undef expm1
#endif
#define expm1 boostRG_expm1
#ifdef expm1f
#undef expm1f
#endif
#define expm1f boostRG_expm1f
#ifdef expm1l
#undef expm1l
#endif
#define expm1l boostRG_expm1l

#if 0
#ifdef fdim
#undef fdim
#endif
#define fdim boostRG_fdim
#ifdef fdimf
#undef fdimf
#endif
#define fdimf boostRG_fdimf
#ifdef fdiml
#undef fdiml
#endif
#define fdiml boostRG_fdiml
#ifdef acosh
#undef acosh
#endif
#define fma boostRG_fma
#ifdef fmaf
#undef fmaf
#endif
#define fmaf boostRG_fmaf
#ifdef fmal
#undef fmal
#endif
#define fmal boostRG_fmal
#endif

#ifdef fmax
#undef fmax
#endif
#define fmax boostRG_fmax
#ifdef fmaxf
#undef fmaxf
#endif
#define fmaxf boostRG_fmaxf
#ifdef fmaxl
#undef fmaxl
#endif
#define fmaxl boostRG_fmaxl

#ifdef fmin
#undef fmin
#endif
#define fmin boostRG_fmin
#ifdef fminf
#undef fminf
#endif
#define fminf boostRG_fminf
#ifdef fminl
#undef fminl
#endif
#define fminl boostRG_fminl

#ifdef hypot
#undef hypot
#endif
#define hypot boostRG_hypot
#ifdef hypotf
#undef hypotf
#endif
#define hypotf boostRG_hypotf
#ifdef hypotl
#undef hypotl
#endif
#define hypotl boostRG_hypotl

#if 0
#ifdef ilogb
#undef ilogb
#endif
#define ilogb boostRG_ilogb
#ifdef ilogbf
#undef ilogbf
#endif
#define ilogbf boostRG_ilogbf
#ifdef ilogbl
#undef ilogbl
#endif
#define ilogbl boostRG_ilogbl
#endif

#ifdef lgamma
#undef lgamma
#endif
#define lgamma boostRG_lgamma
#ifdef lgammaf
#undef lgammaf
#endif
#define lgammaf boostRG_lgammaf
#ifdef lgammal
#undef lgammal
#endif
#define lgammal boostRG_lgammal

#ifdef BOOST_HAS_LONG_LONG
#if 0
#ifdef llrint
#undef llrint
#endif
#define llrint boostRG_llrint
#ifdef llrintf
#undef llrintf
#endif
#define llrintf boostRG_llrintf
#ifdef llrintl
#undef llrintl
#endif
#define llrintl boostRG_llrintl
#endif
#ifdef llround
#undef llround
#endif
#define llround boostRG_llround
#ifdef llroundf
#undef llroundf
#endif
#define llroundf boostRG_llroundf
#ifdef llroundl
#undef llroundl
#endif
#define llroundl boostRG_llroundl
#endif

#ifdef log1p
#undef log1p
#endif
#define log1p boostRG_log1p
#ifdef log1pf
#undef log1pf
#endif
#define log1pf boostRG_log1pf
#ifdef log1pl
#undef log1pl
#endif
#define log1pl boostRG_log1pl

#if 0
#ifdef log2
#undef log2
#endif
#define log2 boostRG_log2
#ifdef log2f
#undef log2f
#endif
#define log2f boostRG_log2f
#ifdef log2l
#undef log2l
#endif
#define log2l boostRG_log2l

#ifdef logb
#undef logb
#endif
#define logb boostRG_logb
#ifdef logbf
#undef logbf
#endif
#define logbf boostRG_logbf
#ifdef logbl
#undef logbl
#endif
#define logbl boostRG_logbl

#ifdef lrint
#undef lrint
#endif
#define lrint boostRG_lrint
#ifdef lrintf
#undef lrintf
#endif
#define lrintf boostRG_lrintf
#ifdef lrintl
#undef lrintl
#endif
#define lrintl boostRG_lrintl
#endif

#ifdef lround
#undef lround
#endif
#define lround boostRG_lround
#ifdef lroundf
#undef lroundf
#endif
#define lroundf boostRG_lroundf
#ifdef lroundl
#undef lroundl
#endif
#define lroundl boostRG_lroundl

#if 0
#ifdef nan
#undef nan
#endif
#define nan boostRG_nan
#ifdef nanf
#undef nanf
#endif
#define nanf boostRG_nanf
#ifdef nanl
#undef nanl
#endif
#define nanl boostRG_nanl

#ifdef nearbyint
#undef nearbyint
#endif
#define nearbyint boostRG_nearbyint
#ifdef nearbyintf
#undef nearbyintf
#endif
#define nearbyintf boostRG_nearbyintf
#ifdef nearbyintl
#undef nearbyintl
#endif
#define nearbyintl boostRG_nearbyintl
#endif

#ifdef nextafter
#undef nextafter
#endif
#define nextafter boostRG_nextafter
#ifdef nextafterf
#undef nextafterf
#endif
#define nextafterf boostRG_nextafterf
#ifdef nextafterl
#undef nextafterl
#endif
#define nextafterl boostRG_nextafterl

#ifdef nexttoward
#undef nexttoward
#endif
#define nexttoward boostRG_nexttoward
#ifdef nexttowardf
#undef nexttowardf
#endif
#define nexttowardf boostRG_nexttowardf
#ifdef nexttowardl
#undef nexttowardl
#endif
#define nexttowardl boostRG_nexttowardl

#if 0
#ifdef remainder
#undef remainder
#endif
#define remainder boostRG_remainder
#ifdef remainderf
#undef remainderf
#endif
#define remainderf boostRG_remainderf
#ifdef remainderl
#undef remainderl
#endif
#define remainderl boostRG_remainderl

#ifdef remquo
#undef remquo
#endif
#define remquo boostRG_remquo
#ifdef remquof
#undef remquof
#endif
#define remquof boostRG_remquof
#ifdef remquol
#undef remquol
#endif
#define remquol boostRG_remquol

#ifdef rint
#undef rint
#endif
#define rint boostRG_rint
#ifdef rintf
#undef rintf
#endif
#define rintf boostRG_rintf
#ifdef rintl
#undef rintl
#endif
#define rintl boostRG_rintl
#endif

#ifdef round
#undef round
#endif
#define round boostRG_round
#ifdef roundf
#undef roundf
#endif
#define roundf boostRG_roundf
#ifdef roundl
#undef roundl
#endif
#define roundl boostRG_roundl

#if 0
#ifdef scalbln
#undef scalbln
#endif
#define scalbln boostRG_scalbln
#ifdef scalblnf
#undef scalblnf
#endif
#define scalblnf boostRG_scalblnf
#ifdef scalblnl
#undef scalblnl
#endif
#define scalblnl boostRG_scalblnl

#ifdef scalbn
#undef scalbn
#endif
#define scalbn boostRG_scalbn
#ifdef scalbnf
#undef scalbnf
#endif
#define scalbnf boostRG_scalbnf
#ifdef scalbnl
#undef scalbnl
#endif
#define scalbnl boostRG_scalbnl
#endif

#ifdef tgamma
#undef tgamma
#endif
#define tgamma boostRG_tgamma
#ifdef tgammaf
#undef tgammaf
#endif
#define tgammaf boostRG_tgammaf
#ifdef tgammal
#undef tgammal
#endif
#define tgammal boostRG_tgammal

#ifdef trunc
#undef trunc
#endif
#define trunc boostRG_trunc
#ifdef truncf
#undef truncf
#endif
#define truncf boostRG_truncf
#ifdef truncl
#undef truncl
#endif
#define truncl boostRG_truncl

// [5.2.1.1] associated Laguerre polynomials:
#ifdef assoc_laguerre
#undef assoc_laguerre
#endif
#define assoc_laguerre boostRG_assoc_laguerre
#ifdef assoc_laguerref
#undef assoc_laguerref
#endif
#define assoc_laguerref boostRG_assoc_laguerref
#ifdef assoc_laguerrel
#undef assoc_laguerrel
#endif
#define assoc_laguerrel boostRG_assoc_laguerrel

// [5.2.1.2] associated Legendre functions:
#ifdef assoc_legendre
#undef assoc_legendre
#endif
#define assoc_legendre boostRG_assoc_legendre
#ifdef assoc_legendref
#undef assoc_legendref
#endif
#define assoc_legendref boostRG_assoc_legendref
#ifdef assoc_legendrel
#undef assoc_legendrel
#endif
#define assoc_legendrel boostRG_assoc_legendrel

// [5.2.1.3] beta function:
#ifdef beta
#undef beta
#endif
#define beta boostRG_beta
#ifdef betaf
#undef betaf
#endif
#define betaf boostRG_betaf
#ifdef betal
#undef betal
#endif
#define betal boostRG_betal

// [5.2.1.4] (complete) elliptic integral of the first kind:
#ifdef comp_ellint_1
#undef comp_ellint_1
#endif
#define comp_ellint_1 boostRG_comp_ellint_1
#ifdef comp_ellint_1f
#undef comp_ellint_1f
#endif
#define comp_ellint_1f boostRG_comp_ellint_1f
#ifdef comp_ellint_1l
#undef comp_ellint_1l
#endif
#define comp_ellint_1l boostRG_comp_ellint_1l

// [5.2.1.5] (complete) elliptic integral of the second kind:
#ifdef comp_ellint_2
#undef comp_ellint_2
#endif
#define comp_ellint_2 boostRG_comp_ellint_2
#ifdef comp_ellint_2f
#undef comp_ellint_2f
#endif
#define comp_ellint_2f boostRG_comp_ellint_2f
#ifdef comp_ellint_2l
#undef comp_ellint_2l
#endif
#define comp_ellint_2l boostRG_comp_ellint_2l

// [5.2.1.6] (complete) elliptic integral of the third kind:
#ifdef comp_ellint_3
#undef comp_ellint_3
#endif
#define comp_ellint_3 boostRG_comp_ellint_3
#ifdef comp_ellint_3f
#undef comp_ellint_3f
#endif
#define comp_ellint_3f boostRG_comp_ellint_3f
#ifdef comp_ellint_3l
#undef comp_ellint_3l
#endif
#define comp_ellint_3l boostRG_comp_ellint_3l

#if 0
// [5.2.1.7] confluent hypergeometric functions:
#ifdef conf_hyper
#undef conf_hyper
#endif
#define conf_hyper boostRG_conf_hyper
#ifdef conf_hyperf
#undef conf_hyperf
#endif
#define conf_hyperf boostRG_conf_hyperf
#ifdef conf_hyperl
#undef conf_hyperl
#endif
#define conf_hyperl boostRG_conf_hyperl
#endif

// [5.2.1.8] regular modified cylindrical Bessel functions:
#ifdef cyl_bessel_i
#undef cyl_bessel_i
#endif
#define cyl_bessel_i boostRG_cyl_bessel_i
#ifdef cyl_bessel_if
#undef cyl_bessel_if
#endif
#define cyl_bessel_if boostRG_cyl_bessel_if
#ifdef cyl_bessel_il
#undef cyl_bessel_il
#endif
#define cyl_bessel_il boostRG_cyl_bessel_il

// [5.2.1.9] cylindrical Bessel functions (of the first kind):
#ifdef cyl_bessel_j
#undef cyl_bessel_j
#endif
#define cyl_bessel_j boostRG_cyl_bessel_j
#ifdef cyl_bessel_jf
#undef cyl_bessel_jf
#endif
#define cyl_bessel_jf boostRG_cyl_bessel_jf
#ifdef cyl_bessel_jl
#undef cyl_bessel_jl
#endif
#define cyl_bessel_jl boostRG_cyl_bessel_jl

// [5.2.1.10] irregular modified cylindrical Bessel functions:
#ifdef cyl_bessel_k
#undef cyl_bessel_k
#endif
#define cyl_bessel_k boostRG_cyl_bessel_k
#ifdef cyl_bessel_kf
#undef cyl_bessel_kf
#endif
#define cyl_bessel_kf boostRG_cyl_bessel_kf
#ifdef cyl_bessel_kl
#undef cyl_bessel_kl
#endif
#define cyl_bessel_kl boostRG_cyl_bessel_kl

// [5.2.1.11] cylindrical Neumann functions BOOST_MATH_C99_THROW_SPEC;
// cylindrical Bessel functions (of the second kind):
#ifdef cyl_neumann
#undef cyl_neumann
#endif
#define cyl_neumann boostRG_cyl_neumann
#ifdef cyl_neumannf
#undef cyl_neumannf
#endif
#define cyl_neumannf boostRG_cyl_neumannf
#ifdef cyl_neumannl
#undef cyl_neumannl
#endif
#define cyl_neumannl boostRG_cyl_neumannl

// [5.2.1.12] (incomplete) elliptic integral of the first kind:
#ifdef ellint_1
#undef ellint_1
#endif
#define ellint_1 boostRG_ellint_1
#ifdef ellint_1f
#undef ellint_1f
#endif
#define ellint_1f boostRG_ellint_1f
#ifdef ellint_1l
#undef ellint_1l
#endif
#define ellint_1l boostRG_ellint_1l

// [5.2.1.13] (incomplete) elliptic integral of the second kind:
#ifdef ellint_2
#undef ellint_2
#endif
#define ellint_2 boostRG_ellint_2
#ifdef ellint_2f
#undef ellint_2f
#endif
#define ellint_2f boostRG_ellint_2f
#ifdef ellint_2l
#undef ellint_2l
#endif
#define ellint_2l boostRG_ellint_2l

// [5.2.1.14] (incomplete) elliptic integral of the third kind:
#ifdef ellint_3
#undef ellint_3
#endif
#define ellint_3 boostRG_ellint_3
#ifdef ellint_3f
#undef ellint_3f
#endif
#define ellint_3f boostRG_ellint_3f
#ifdef ellint_3l
#undef ellint_3l
#endif
#define ellint_3l boostRG_ellint_3l

// [5.2.1.15] exponential integral:
#ifdef expint
#undef expint
#endif
#define expint boostRG_expint
#ifdef expintf
#undef expintf
#endif
#define expintf boostRG_expintf
#ifdef expintl
#undef expintl
#endif
#define expintl boostRG_expintl

// [5.2.1.16] Hermite polynomials:
#ifdef hermite
#undef hermite
#endif
#define hermite boostRG_hermite
#ifdef hermitef
#undef hermitef
#endif
#define hermitef boostRG_hermitef
#ifdef hermitel
#undef hermitel
#endif
#define hermitel boostRG_hermitel

#if 0
// [5.2.1.17] hypergeometric functions:
#ifdef hyperg
#undef hyperg
#endif
#define hyperg boostRG_hyperg
#ifdef hypergf
#undef hypergf
#endif
#define hypergf boostRG_hypergf
#ifdef hypergl
#undef hypergl
#endif
#define hypergl boostRG_hypergl
#endif

// [5.2.1.18] Laguerre polynomials:
#ifdef laguerre
#undef laguerre
#endif
#define laguerre boostRG_laguerre
#ifdef laguerref
#undef laguerref
#endif
#define laguerref boostRG_laguerref
#ifdef laguerrel
#undef laguerrel
#endif
#define laguerrel boostRG_laguerrel

// [5.2.1.19] Legendre polynomials:
#ifdef legendre
#undef legendre
#endif
#define legendre boostRG_legendre
#ifdef legendref
#undef legendref
#endif
#define legendref boostRG_legendref
#ifdef legendrel
#undef legendrel
#endif
#define legendrel boostRG_legendrel

// [5.2.1.20] Riemann zeta function:
#ifdef riemann_zeta
#undef riemann_zeta
#endif
#define riemann_zeta boostRG_riemann_zeta
#ifdef riemann_zetaf
#undef riemann_zetaf
#endif
#define riemann_zetaf boostRG_riemann_zetaf
#ifdef riemann_zetal
#undef riemann_zetal
#endif
#define riemann_zetal boostRG_riemann_zetal

// [5.2.1.21] spherical Bessel functions (of the first kind):
#ifdef sph_bessel
#undef sph_bessel
#endif
#define sph_bessel boostRG_sph_bessel
#ifdef sph_besself
#undef sph_besself
#endif
#define sph_besself boostRG_sph_besself
#ifdef sph_bessell
#undef sph_bessell
#endif
#define sph_bessell boostRG_sph_bessell

// [5.2.1.22] spherical associated Legendre functions:
#ifdef sph_legendre
#undef sph_legendre
#endif
#define sph_legendre boostRG_sph_legendre
#ifdef sph_legendref
#undef sph_legendref
#endif
#define sph_legendref boostRG_sph_legendref
#ifdef sph_legendrel
#undef sph_legendrel
#endif
#define sph_legendrel boostRG_sph_legendrel

// [5.2.1.23] spherical Neumann functions BOOST_MATH_C99_THROW_SPEC;
// spherical Bessel functions (of the second kind):
#ifdef sph_neumann
#undef sph_neumann
#endif
#define sph_neumann boostRG_sph_neumann
#ifdef sph_neumannf
#undef sph_neumannf
#endif
#define sph_neumannf boostRG_sph_neumannf
#ifdef sph_neumannl
#undef sph_neumannl
#endif
#define sph_neumannl boostRG_sph_neumannl

#endif // BOOST_MATH_C_MACROS_IPP
