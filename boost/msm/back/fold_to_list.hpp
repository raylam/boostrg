// Copyright 2008 Christophe Henry
// henry UNDERSCORE christophe AT hotmail DOT com
// This is taken from Boost.Proto's documentation
// Copyright for the original version:
// Copyright 2008 Eric Niebler. Distributed
// under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_MSM_BACK_FOLD_TO_LIST_H
#define BOOST_MSM_BACK_FOLD_TO_LIST_H

#include <boost/msm/proto_config.hpp>
#include <boost/proto/core.hpp>
#include <boost/proto/transform.hpp>
#include <boost/msm/msm_grammar.hpp>
#include <boost/fusion/container/list/cons.hpp>

namespace boostRG { namespace msm { namespace back
{
 struct state_copy_tag
 {
 };

template<class X = proto::is_proto_expr>
struct define_states_creation
{
   BOOST_PROTO_BASIC_EXTENDS(
       proto::terminal<state_copy_tag>::type
     , define_states_creation
     , boostRG::msm::msm_domain
   )
};

define_states_creation<> const states_ = {{{}}};

 struct FoldToList
  : ::boostRG::proto::or_<
        // Don't add the states_ terminal to the list
        ::boostRG::proto::when<
            ::boostRG::proto::terminal< state_copy_tag >
          , ::boostRG::proto::_state
        >
        // Put all other terminals at the head of the
        // list that we're building in the "state" parameter
        // first states for the eUML states
      , ::boostRG::proto::when<
            ::boostRG::proto::terminal< state_tag >
            , boostRG::fusion::cons< ::boostRG::proto::_, ::boostRG::proto::_state>(
                ::boostRG::proto::_, ::boostRG::proto::_state
            )
        >
        // then states from other front-ends
      , ::boostRG::proto::when<
      ::boostRG::proto::terminal< proto::_ >
            , boostRG::fusion::cons< ::boostRG::proto::_value, ::boostRG::proto::_state>(
                ::boostRG::proto::_value, ::boostRG::proto::_state
    )
        >
        // For left-shift operations, first fold the right
        // child to a list using the current state. Use
        // the result as the state parameter when folding
        // the left child to a list.
      , ::boostRG::proto::when<
            ::boostRG::proto::shift_left<FoldToList, FoldToList>
          , FoldToList(
                ::boostRG::proto::_left
              , ::boostRG::proto::call<FoldToList( ::boostRG::proto::_right, ::boostRG::proto::_state )>
            )
        >
    >
 {};

}}}

#endif //BOOST_MSM_BACK_FOLD_TO_LIST_H

