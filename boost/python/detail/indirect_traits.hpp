// Copyright David Abrahams 2004. Distributed under the Boost
// Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
#ifndef INDIRECT_TRAITS_DWA2004915_HPP
# define INDIRECT_TRAITS_DWA2004915_HPP

# include <boost/detail/indirect_traits.hpp>

namespace boostRG { namespace python {
namespace indirect_traits = boostRG::detail::indirect_traits;
}} // namespace boostRG::python::detail

#endif // INDIRECT_TRAITS_DWA2004915_HPP
