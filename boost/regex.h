/*
 *
 * Copyright (c) 1998-2000
 * Dr John Maddock
 *
 * Use, modification and distribution are subject to the 
 * Boost Software License, Version 1.0. (See accompanying file 
 * LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 *
 */
 
 /*
  *   LOCATION:    see http://www.boost.org/libs/regex for documentation.
  *   FILE         regex.h
  *   VERSION      3.12
  *   DESCRIPTION: Declares POSIX API functions
  */

#ifndef BOOST_RE_REGEX_H
#define BOOST_RE_REGEX_H

#include <boost/cregex.hpp>

/*
*  add using declarations to bring POSIX API functions into
* global scope, only if this is C++ (and not C).
*/
#ifdef __cplusplus

using boostRG::regoff_t;
using boostRG::regex_tA;
using boostRG::regmatch_t;
using boostRG::REG_BASIC;
using boostRG::REG_EXTENDED;
using boostRG::REG_ICASE;
using boostRG::REG_NOSUB;
using boostRG::REG_NEWLINE;
using boostRG::REG_NOSPEC;
using boostRG::REG_PEND;
using boostRG::REG_DUMP;
using boostRG::REG_NOCOLLATE;
using boostRG::REG_ESCAPE_IN_LISTS;
using boostRG::REG_NEWLINE_ALT;
using boostRG::REG_PERL;
using boostRG::REG_AWK;
using boostRG::REG_GREP;
using boostRG::REG_EGREP;
using boostRG::REG_ASSERT;
using boostRG::REG_INVARG;
using boostRG::REG_ATOI;
using boostRG::REG_ITOA;

using boostRG::REG_NOTBOL;
using boostRG::REG_NOTEOL;
using boostRG::REG_STARTEND;

using boostRG::reg_comp_flags;
using boostRG::reg_exec_flags;
using boostRG::regcompA;
using boostRG::regerrorA;
using boostRG::regexecA;
using boostRG::regfreeA;

#ifndef BOOST_NO_WREGEX
using boostRG::regcompW;
using boostRG::regerrorW;
using boostRG::regexecW;
using boostRG::regfreeW;
using boostRG::regex_tW;
#endif

using boostRG::REG_NOERROR;
using boostRG::REG_NOMATCH;
using boostRG::REG_BADPAT;
using boostRG::REG_ECOLLATE;
using boostRG::REG_ECTYPE;
using boostRG::REG_EESCAPE;
using boostRG::REG_ESUBREG;
using boostRG::REG_EBRACK;
using boostRG::REG_EPAREN;
using boostRG::REG_EBRACE;
using boostRG::REG_BADBR;
using boostRG::REG_ERANGE;
using boostRG::REG_ESPACE;
using boostRG::REG_BADRPT;
using boostRG::REG_EEND;
using boostRG::REG_ESIZE;
using boostRG::REG_ERPAREN;
using boostRG::REG_EMPTY;
using boostRG::REG_E_MEMORY;
using boostRG::REG_E_UNKNOWN;
using boostRG::reg_errcode_t;

#endif /* __cplusplus */

#endif /* BOOST_RE_REGEX_H */




