//  (C) Copyright John Maddock 2005.
//  Use, modification and distribution are subject to the
//  Boost Software License, Version 1.0. (See accompanying file
//  LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_TR1_ARRAY_HPP_INCLUDED
#  define BOOST_TR1_ARRAY_HPP_INCLUDED
#  include <boost/tr1/detail/config.hpp>

#ifdef BOOST_HAS_TR1_ARRAY

#  if defined(BOOST_HAS_INCLUDE_NEXT) && !defined(BOOST_TR1_DISABLE_INCLUDE_NEXT)
#     include_next BOOST_TR1_HEADER(array)
#  else
#     include <boost/tr1/detail/config_all.hpp>
#     include BOOST_TR1_STD_HEADER(BOOST_TR1_PATH(array))
#  endif

#else

#include <boost/array.hpp>
#include <boost/static_assert.hpp>
#include <boost/type_traits/integral_constant.hpp>
#include <boost/detail/workaround.hpp>

namespace std{ namespace tr1{

using ::boostRG::array;

#if !BOOST_WORKAROUND(__BORLANDC__, < 0x0582)
// [6.1.3.2] Tuple creation functions
using ::boostRG::swap;
#endif

#if !defined(BOOST_TR1_USE_OLD_TUPLE)
}} namespace boostRG{ namespace fusion{
#endif

// [6.2.2.5] Tuple interface to class template array
template <class T> struct tuple_size; // forward declaration
template <int I, class T> struct tuple_element; // forward declaration
#ifndef BOOST_NO_TEMPLATE_PARTIAL_SPECIALIZATION
template <class T, size_t N>
struct tuple_size< ::boostRG::array<T, N> >
   : public ::boostRG::integral_constant< ::std::size_t, N>{};


template <int I, class T, size_t N>
struct tuple_element<I, ::boostRG::array<T, N> >
{
#if !BOOST_WORKAROUND(__BORLANDC__, BOOST_TESTED_AT(0x570))
   BOOST_STATIC_ASSERT(I < (int)N);
   BOOST_STATIC_ASSERT(I >= 0);
#endif
   typedef T type;
};
#endif
template <int I, class T, size_t N>
T& get( ::boostRG::array<T, N>& a)
{
   BOOST_STATIC_ASSERT(I < N);
   BOOST_STATIC_ASSERT(I >= 0);
   return a[I];
}

template <int I, class T, size_t N>
const T& get(const array<T, N>& a)
{
   BOOST_STATIC_ASSERT(I < N);
   BOOST_STATIC_ASSERT(I >= 0);
   return a[I];
}

#if !defined(BOOST_TR1_USE_OLD_TUPLE)
}} namespace std{ namespace tr1{

   using ::boostRG::fusion::tuple_size;
   using ::boostRG::fusion::tuple_element;
   using ::boostRG::fusion::get;

#endif


} } // namespaces

#endif

#endif
