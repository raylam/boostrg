//  (C) Copyright John Maddock 2008.
//  Use, modification and distribution are subject to the
//  Boost Software License, Version 1.0. (See accompanying file
//  LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_TR1_CMATH_HPP_INCLUDED
#  define BOOST_TR1_CMATH_HPP_INCLUDED
#  include <boost/tr1/detail/config.hpp>

#ifdef BOOST_HAS_TR1_CMATH

#  if defined(BOOST_HAS_INCLUDE_NEXT) && !defined(BOOST_TR1_DISABLE_INCLUDE_NEXT)
#     include_next BOOST_TR1_HEADER(cmath)
#  else
#     include <boost/tr1/detail/config_all.hpp>
#     include BOOST_TR1_HEADER(cmath)
#  endif

#else

#include <boost/math/tr1.hpp>

namespace std{ namespace tr1{

using boostRG::math::tr1::assoc_laguerre;
using boostRG::math::tr1::assoc_laguerref;
using boostRG::math::tr1::assoc_laguerrel;
// [5.2.1.2] associated Legendre functions:
using boostRG::math::tr1::assoc_legendre;
using boostRG::math::tr1::assoc_legendref;
using boostRG::math::tr1::assoc_legendrel;
// [5.2.1.3] beta function:
using boostRG::math::tr1::beta;
using boostRG::math::tr1::betaf;
using boostRG::math::tr1::betal;
// [5.2.1.4] (complete) elliptic integral of the first kind:
using boostRG::math::tr1::comp_ellint_1;
using boostRG::math::tr1::comp_ellint_1f;
using boostRG::math::tr1::comp_ellint_1l;
// [5.2.1.5] (complete) elliptic integral of the second kind:
using boostRG::math::tr1::comp_ellint_2;
using boostRG::math::tr1::comp_ellint_2f;
using boostRG::math::tr1::comp_ellint_2l;
// [5.2.1.6] (complete) elliptic integral of the third kind:
using boostRG::math::tr1::comp_ellint_3;
using boostRG::math::tr1::comp_ellint_3f;
using boostRG::math::tr1::comp_ellint_3l;
#if 0
// [5.2.1.7] confluent hypergeometric functions:
using boostRG::math::tr1::conf_hyperg;
using boostRG::math::tr1::conf_hypergf;
using boostRG::math::tr1::conf_hypergl;
#endif
// [5.2.1.8] regular modified cylindrical Bessel functions:
using boostRG::math::tr1::cyl_bessel_i;
using boostRG::math::tr1::cyl_bessel_if;
using boostRG::math::tr1::cyl_bessel_il;
// [5.2.1.9] cylindrical Bessel functions (of the first kind):
using boostRG::math::tr1::cyl_bessel_j;
using boostRG::math::tr1::cyl_bessel_jf;
using boostRG::math::tr1::cyl_bessel_jl;
// [5.2.1.10] irregular modified cylindrical Bessel functions:
using boostRG::math::tr1::cyl_bessel_k;
using boostRG::math::tr1::cyl_bessel_kf;
using boostRG::math::tr1::cyl_bessel_kl;
// [5.2.1.11] cylindrical Neumann functions;
// cylindrical Bessel functions (of the second kind):
using boostRG::math::tr1::cyl_neumann;
using boostRG::math::tr1::cyl_neumannf;
using boostRG::math::tr1::cyl_neumannl;
// [5.2.1.12] (incomplete) elliptic integral of the first kind:
using boostRG::math::tr1::ellint_1;
using boostRG::math::tr1::ellint_1f;
using boostRG::math::tr1::ellint_1l;
// [5.2.1.13] (incomplete) elliptic integral of the second kind:
using boostRG::math::tr1::ellint_2;
using boostRG::math::tr1::ellint_2f;
using boostRG::math::tr1::ellint_2l;
// [5.2.1.14] (incomplete) elliptic integral of the third kind:
using boostRG::math::tr1::ellint_3;
using boostRG::math::tr1::ellint_3f;
using boostRG::math::tr1::ellint_3l;
// [5.2.1.15] exponential integral:
using boostRG::math::tr1::expint;
using boostRG::math::tr1::expintf;
using boostRG::math::tr1::expintl;
// [5.2.1.16] Hermite polynomials:
using boostRG::math::tr1::hermite;
using boostRG::math::tr1::hermitef;
using boostRG::math::tr1::hermitel;
#if 0
// [5.2.1.17] hypergeometric functions:
using boostRG::math::tr1::hyperg;
using boostRG::math::tr1::hypergf;
using boostRG::math::tr1::hypergl;
#endif
// [5.2.1.18] Laguerre polynomials:
using boostRG::math::tr1::laguerre;
using boostRG::math::tr1::laguerref;
using boostRG::math::tr1::laguerrel;
// [5.2.1.19] Legendre polynomials:
using boostRG::math::tr1::legendre;
using boostRG::math::tr1::legendref;
using boostRG::math::tr1::legendrel;
// [5.2.1.20] Riemann zeta function:
using boostRG::math::tr1::riemann_zeta;
using boostRG::math::tr1::riemann_zetaf;
using boostRG::math::tr1::riemann_zetal;
// [5.2.1.21] spherical Bessel functions (of the first kind):
using boostRG::math::tr1::sph_bessel;
using boostRG::math::tr1::sph_besself;
using boostRG::math::tr1::sph_bessell;
// [5.2.1.22] spherical associated Legendre functions:
using boostRG::math::tr1::sph_legendre;
using boostRG::math::tr1::sph_legendref;
using boostRG::math::tr1::sph_legendrel;
// [5.2.1.23] spherical Neumann functions;
// spherical Bessel functions (of the second kind):
using boostRG::math::tr1::sph_neumann;
using boostRG::math::tr1::sph_neumannf;
using boostRG::math::tr1::sph_neumannl;

// types
using boostRG::math::tr1::double_t;
using boostRG::math::tr1::float_t;
// functions
using boostRG::math::tr1::acosh;
using boostRG::math::tr1::acoshf;
using boostRG::math::tr1::acoshl;
using boostRG::math::tr1::asinh;
using boostRG::math::tr1::asinhf;
using boostRG::math::tr1::asinhl;
using boostRG::math::tr1::atanh;
using boostRG::math::tr1::atanhf;
using boostRG::math::tr1::atanhl;
using boostRG::math::tr1::cbrt;
using boostRG::math::tr1::cbrtf;
using boostRG::math::tr1::cbrtl;
using boostRG::math::tr1::copysign;
using boostRG::math::tr1::copysignf;
using boostRG::math::tr1::copysignl;
using boostRG::math::tr1::erf;
using boostRG::math::tr1::erff;
using boostRG::math::tr1::erfl;
using boostRG::math::tr1::erfc;
using boostRG::math::tr1::erfcf;
using boostRG::math::tr1::erfcl;
#if 0
using boostRG::math::tr1::exp2;
using boostRG::math::tr1::exp2f;
using boostRG::math::tr1::exp2l;
#endif
using boostRG::math::tr1::expm1;
using boostRG::math::tr1::expm1f;
using boostRG::math::tr1::expm1l;
#if 0
using boostRG::math::tr1::fdim;
using boostRG::math::tr1::fdimf;
using boostRG::math::tr1::fdiml;
using boostRG::math::tr1::fma;
using boostRG::math::tr1::fmaf;
using boostRG::math::tr1::fmal;
#endif
using boostRG::math::tr1::fmax;
using boostRG::math::tr1::fmaxf;
using boostRG::math::tr1::fmaxl;
using boostRG::math::tr1::fmin;
using boostRG::math::tr1::fminf;
using boostRG::math::tr1::fminl;
using boostRG::math::tr1::hypot;
using boostRG::math::tr1::hypotf;
using boostRG::math::tr1::hypotl;
#if 0
using boostRG::math::tr1::ilogb;
using boostRG::math::tr1::ilogbf;
using boostRG::math::tr1::ilogbl;
#endif
using boostRG::math::tr1::lgamma;
using boostRG::math::tr1::lgammaf;
using boostRG::math::tr1::lgammal;
#if 0
using boostRG::math::tr1::llrint;
using boostRG::math::tr1::llrintf;
using boostRG::math::tr1::llrintl;
#endif
using boostRG::math::tr1::llround;
using boostRG::math::tr1::llroundf;
using boostRG::math::tr1::llroundl;
using boostRG::math::tr1::log1p;
using boostRG::math::tr1::log1pf;
using boostRG::math::tr1::log1pl;
#if 0
using boostRG::math::tr1::log2;
using boostRG::math::tr1::log2f;
using boostRG::math::tr1::log2l;
using boostRG::math::tr1::logb;
using boostRG::math::tr1::logbf;
using boostRG::math::tr1::logbl;
using boostRG::math::tr1::lrint;
using boostRG::math::tr1::lrintf;
using boostRG::math::tr1::lrintl;
#endif
using boostRG::math::tr1::lround;
using boostRG::math::tr1::lroundf;
using boostRG::math::tr1::lroundl;
#if 0
using boostRG::math::tr1::nan;
using boostRG::math::tr1::nanf;
using boostRG::math::tr1::nanl;
using boostRG::math::tr1::nearbyint;
using boostRG::math::tr1::nearbyintf;
using boostRG::math::tr1::nearbyintl;
#endif
using boostRG::math::tr1::nextafter;
using boostRG::math::tr1::nextafterf;
using boostRG::math::tr1::nextafterl;
using boostRG::math::tr1::nexttoward;
using boostRG::math::tr1::nexttowardf;
using boostRG::math::tr1::nexttowardl;
#if 0
using boostRG::math::tr1::remainder;
using boostRG::math::tr1::remainderf;
using boostRG::math::tr1::remainderl;
using boostRG::math::tr1::remquo;
using boostRG::math::tr1::remquof;
using boostRG::math::tr1::remquol;
using boostRG::math::tr1::rint;
using boostRG::math::tr1::rintf;
using boostRG::math::tr1::rintl;
#endif
using boostRG::math::tr1::round;
using boostRG::math::tr1::roundf;
using boostRG::math::tr1::roundl;
#if 0
using boostRG::math::tr1::scalbln;
using boostRG::math::tr1::scalblnf;
using boostRG::math::tr1::scalblnl;
using boostRG::math::tr1::scalbn;
using boostRG::math::tr1::scalbnf;
using boostRG::math::tr1::scalbnl;
#endif
using boostRG::math::tr1::tgamma;
using boostRG::math::tr1::tgammaf;
using boostRG::math::tr1::tgammal;
using boostRG::math::tr1::trunc;
using boostRG::math::tr1::truncf;
using boostRG::math::tr1::truncl;
// C99 macros defined as C++ templates
using boostRG::math::tr1::signbit;
using boostRG::math::tr1::fpclassify;
using boostRG::math::tr1::isfinite;
using boostRG::math::tr1::isinf;
using boostRG::math::tr1::isnan;
using boostRG::math::tr1::isnormal;
#if 0
using boostRG::math::tr1::isgreater;
using boostRG::math::tr1::isgreaterequal;
using boostRG::math::tr1::isless;
using boostRG::math::tr1::islessequal;
using boostRG::math::tr1::islessgreater;
using boostRG::math::tr1::isunordered;
#endif
} } // namespaces

#endif // BOOST_HAS_TR1_CMATH

#endif // BOOST_TR1_CMATH_HPP_INCLUDED
