//  (C) Copyright John Maddock 2005.
//  Use, modification and distribution are subject to the
//  Boost Software License, Version 1.0. (See accompanying file
//  LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_TR1_REGEX_HPP_INCLUDED
#  define BOOST_TR1_REGEX_HPP_INCLUDED
#  include <boost/tr1/detail/config.hpp>

#ifdef BOOST_HAS_TR1_REGEX

#  if defined(BOOST_HAS_INCLUDE_NEXT) && !defined(BOOST_TR1_DISABLE_INCLUDE_NEXT)
#     include_next BOOST_TR1_HEADER(regex)
#  else
#     include <boost/tr1/detail/config_all.hpp>
#     include BOOST_TR1_STD_HEADER(BOOST_TR1_PATH(regex))
#  endif

#else

#include <boost/regex.hpp>

namespace std{ namespace tr1{

// [7.5] Regex constants
namespace regex_constants {

using ::boostRG::regex_constants::syntax_option_type;
using ::boostRG::regex_constants::icase;
using ::boostRG::regex_constants::nosubs;
using ::boostRG::regex_constants::optimize;
using ::boostRG::regex_constants::collate;
using ::boostRG::regex_constants::ECMAScript;
using ::boostRG::regex_constants::basic;
using ::boostRG::regex_constants::extended;
using ::boostRG::regex_constants::awk;
using ::boostRG::regex_constants::grep;
using ::boostRG::regex_constants::egrep;

using ::boostRG::regex_constants::match_flag_type;
using ::boostRG::regex_constants::match_default;
using ::boostRG::regex_constants::match_not_bol;
using ::boostRG::regex_constants::match_not_eol;
using ::boostRG::regex_constants::match_not_bow;
using ::boostRG::regex_constants::match_not_eow;
using ::boostRG::regex_constants::match_any;
using ::boostRG::regex_constants::match_not_null;
using ::boostRG::regex_constants::match_continuous;
using ::boostRG::regex_constants::match_prev_avail;
using ::boostRG::regex_constants::format_default;
using ::boostRG::regex_constants::format_sed;
using ::boostRG::regex_constants::format_no_copy;
using ::boostRG::regex_constants::format_first_only;

using ::boostRG::regex_constants::error_type;
using ::boostRG::regex_constants::error_collate;
using ::boostRG::regex_constants::error_ctype;
using ::boostRG::regex_constants::error_escape;
using ::boostRG::regex_constants::error_backref;
using ::boostRG::regex_constants::error_brack;
using ::boostRG::regex_constants::error_paren;
using ::boostRG::regex_constants::error_brace;
using ::boostRG::regex_constants::error_badbrace;
using ::boostRG::regex_constants::error_range;
using ::boostRG::regex_constants::error_space;
using ::boostRG::regex_constants::error_badrepeat;
using ::boostRG::regex_constants::error_complexity;
using ::boostRG::regex_constants::error_stack;

} // namespace regex_constants

// [7.6] Class regex_error
using ::boostRG::regex_error;

// [7.7] Class template regex_traits
using ::boostRG::regex_traits;

// [7.8] Class template basic_regex
using ::boostRG::basic_regex;
using ::boostRG::regex;
#ifndef BOOST_NO_WREGEX
using ::boostRG::wregex;
#endif

#if !BOOST_WORKAROUND(__BORLANDC__, < 0x0582)
// [7.8.6] basic_regex swap
using ::boostRG::swap;
#endif

// [7.9] Class template sub_match
using ::boostRG::sub_match;

using ::boostRG::csub_match;
#ifndef BOOST_NO_WREGEX
using ::boostRG::wcsub_match;
#endif
using ::boostRG::ssub_match;
#ifndef BOOST_NO_WREGEX
using ::boostRG::wssub_match;
#endif

// [7.10] Class template match_results
using ::boostRG::match_results;
using ::boostRG::cmatch;
#ifndef BOOST_NO_WREGEX
using ::boostRG::wcmatch;
#endif
using ::boostRG::smatch;
#ifndef BOOST_NO_WREGEX
using ::boostRG::wsmatch;
#endif

using ::boostRG::regex_match;

// [7.11.3] Function template regex_search
using ::boostRG::regex_search;

// [7.11.4] Function template regex_replace
using ::boostRG::regex_replace;

// [7.12.1] Class template regex_iterator
using ::boostRG::regex_iterator;
using ::boostRG::cregex_iterator;
#ifndef BOOST_NO_WREGEX
using ::boostRG::wcregex_iterator;
#endif
using ::boostRG::sregex_iterator;
#ifndef BOOST_NO_WREGEX
using ::boostRG::wsregex_iterator;
#endif

// [7.12.2] Class template regex_token_iterator
using ::boostRG::regex_token_iterator;
using ::boostRG::cregex_token_iterator;
#ifndef BOOST_NO_WREGEX
using ::boostRG::wcregex_token_iterator;
#endif
using ::boostRG::sregex_token_iterator;
#ifndef BOOST_NO_WREGEX
using ::boostRG::wsregex_token_iterator;
#endif

} } // namespaces

#endif

#endif
