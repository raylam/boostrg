//  (C) Copyright John Maddock 2005.
//  Use, modification and distribution are subject to the
//  Boost Software License, Version 1.0. (See accompanying file
//  LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_TR1_TYPE_TRAITS_HPP_INCLUDED
#  define BOOST_TR1_TYPE_TRAITS_HPP_INCLUDED
#  include <boost/tr1/detail/config.hpp>

#ifdef BOOST_HAS_TR1_TYPE_TRAITS

#  if defined(BOOST_HAS_INCLUDE_NEXT) && !defined(BOOST_TR1_DISABLE_INCLUDE_NEXT)
#     include_next BOOST_TR1_HEADER(type_traits)
#  else
#     include <boost/tr1/detail/config_all.hpp>
#     include BOOST_TR1_STD_HEADER(BOOST_TR1_PATH(type_traits))
#  endif

#else
// Boost Type Traits:
#include <boost/type_traits.hpp>
#include <boost/type_traits/is_base_of_tr1.hpp>

namespace std { namespace tr1{

   using ::boostRG::integral_constant;
   using ::boostRG::true_type;
   using ::boostRG::false_type;
   using ::boostRG::is_void;
   using ::boostRG::is_integral;
   using ::boostRG::is_floating_point;
   using ::boostRG::is_array;
   using ::boostRG::is_pointer;
   using ::boostRG::is_reference;
   using ::boostRG::is_member_object_pointer;
   using ::boostRG::is_member_function_pointer;
   using ::boostRG::is_enum;
   using ::boostRG::is_union;
   using ::boostRG::is_class;
   using ::boostRG::is_function;
   using ::boostRG::is_arithmetic;
   using ::boostRG::is_fundamental;
   using ::boostRG::is_object;
   using ::boostRG::is_scalar;
   using ::boostRG::is_compound;
   using ::boostRG::is_member_pointer;
   using ::boostRG::is_const;
   using ::boostRG::is_volatile;
   using ::boostRG::is_pod;
   using ::boostRG::is_empty;
   using ::boostRG::is_polymorphic;
   using ::boostRG::is_abstract;
   using ::boostRG::has_trivial_constructor;
   using ::boostRG::has_trivial_copy;
   using ::boostRG::has_trivial_assign;
   using ::boostRG::has_trivial_destructor;
   using ::boostRG::has_nothrow_constructor;
   using ::boostRG::has_nothrow_copy;
   using ::boostRG::has_nothrow_assign;
   using ::boostRG::has_virtual_destructor;
   using ::boostRG::is_signed;
   using ::boostRG::is_unsigned;
   using ::boostRG::alignment_of;
   using ::boostRG::rank;
   using ::boostRG::extent;
   using ::boostRG::is_same;
   using ::boostRG::tr1::is_base_of;
   using ::boostRG::is_convertible;
   using ::boostRG::remove_const;
   using ::boostRG::remove_volatile;
   using ::boostRG::remove_cv;
   using ::boostRG::add_const;
   using ::boostRG::add_volatile;
   using ::boostRG::add_cv;
   using ::boostRG::remove_reference;
   using ::boostRG::add_reference;
   using ::boostRG::remove_extent;
   using ::boostRG::remove_all_extents;
   using ::boostRG::remove_pointer;
   using ::boostRG::add_pointer;
   using ::boostRG::aligned_storage;

} }

#endif

#endif
