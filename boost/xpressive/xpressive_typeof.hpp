///////////////////////////////////////////////////////////////////////////////
/// \file xpressive_typeof.hpp
/// Type registrations so that xpressive can be used with the Boost.Typeof library.
//
//  Copyright 2008 Eric Niebler. Distributed under the Boost
//  Software License, Version 1.0. (See accompanying file
//  LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_XPRESSIVE_XPRESSIVE_TYPEOF_H
#define BOOST_XPRESSIVE_XPRESSIVE_TYPEOF_H

// MS compatible compilers support #pragma once
#if defined(_MSC_VER) && (_MSC_VER >= 1020)
# pragma once
#endif

#include <boost/config.hpp>
#include <boost/typeof/typeof.hpp>
#ifndef BOOST_NO_STD_LOCALE
# include <boost/typeof/std/locale.hpp>
#endif
#include <boost/proto/proto_typeof.hpp>
#include <boost/xpressive/detail/detail_fwd.hpp>

#include BOOST_TYPEOF_INCREMENT_REGISTRATION_GROUP()

BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::mpl::bool_, (bool))

///////////////////////////////////////////////////////////////////////////////
// Misc.
//
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::set_initializer)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::keeper_tag)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::modifier_tag)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::lookahead_tag)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::lookbehind_tag)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::check_tag)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::mark_tag)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::word_begin)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::word_end)
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::generic_quant_tag, (unsigned int)(unsigned int))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::basic_regex, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::word_boundary, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::value, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::reference, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::local, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::placeholder, (typename)(int)(typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::tracking_ptr, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::regex_impl, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::let_, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::action_arg, (typename)(typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::named_mark, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::sub_match, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::nested_results, (typename))

///////////////////////////////////////////////////////////////////////////////
// Placeholders
//
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::mark_placeholder)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::posix_charset_placeholder)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::assert_bol_placeholder)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::assert_eol_placeholder)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::logical_newline_placeholder)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::self_placeholder)
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::assert_word_placeholder, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::range_placeholder, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::attribute_placeholder, (typename))

///////////////////////////////////////////////////////////////////////////////
// Matchers
//
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::epsilon_matcher)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::true_matcher)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::end_matcher)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::independent_end_matcher)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::any_matcher)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::assert_bos_matcher)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::assert_eos_matcher)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::mark_begin_matcher)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::mark_end_matcher)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::repeat_begin_matcher)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::alternate_end_matcher)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::attr_end_matcher)
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::assert_bol_matcher, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::assert_eol_matcher, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::literal_matcher, (typename)(typename)(typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::string_matcher, (typename)(typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::charset_matcher, (typename)(typename)(typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::logical_newline_matcher, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::mark_matcher, (typename)(typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::repeat_end_matcher, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::alternate_matcher, (typename)(typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::optional_matcher, (typename)(typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::optional_mark_matcher, (typename)(typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::simple_repeat_matcher, (typename)(typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::regex_byref_matcher, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::regex_matcher, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::posix_charset_matcher, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::assert_word_matcher, (typename)(typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::range_matcher, (typename)(typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::keeper_matcher, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::lookahead_matcher, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::lookbehind_matcher, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::set_matcher, (typename)(typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::predicate_matcher, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::action_matcher, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::attr_matcher, (typename)(typename)(typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::attr_begin_matcher, (typename))

///////////////////////////////////////////////////////////////////////////////
// Ops
//
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::op::push)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::op::push_back)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::op::pop)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::op::push_front)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::op::pop_back)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::op::pop_front)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::op::back)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::op::front)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::op::top)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::op::first)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::op::second)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::op::matched)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::op::length)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::op::str)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::op::insert)
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::op::make_pair)
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::op::as, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::op::static_cast_, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::op::dynamic_cast_, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::op::const_cast_, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::op::construct, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::op::throw_, (typename))

///////////////////////////////////////////////////////////////////////////////
// Modifiers
//
BOOST_TYPEOF_REGISTER_TYPE(boostRG::xpressive::detail::icase_modifier)
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::detail::locale_modifier, (typename))

///////////////////////////////////////////////////////////////////////////////
// Traits
//
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::null_regex_traits, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::cpp_regex_traits, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::c_regex_traits, (typename))
BOOST_TYPEOF_REGISTER_TEMPLATE(boostRG::xpressive::regex_traits, (typename)(typename))

#endif
